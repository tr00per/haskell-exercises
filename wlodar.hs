main = putStrLn $ show $ kocha 100000001

kocha :: Integer -> String
kocha 0 = "Kocha"
kocha x = nieKocha (x - 1)

nieKocha :: Integer -> String
nieKocha 0 = "Nie kocha"
nieKocha x = kocha (x - 1)
