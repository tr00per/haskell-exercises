import qualified Data.List as L

main = do
    let a = [1,2,3,1,3,2,5,6,7,8,4,2,5]
        b = nodups a
        b' = L.nub a
    putStrLn $ show a
    putStrLn $ show b
    putStrLn $ show b'
    putStrLn $ show (areNoDups a)
    putStrLn $ show (areNoDups b)
    putStrLn $ show (areNoDups b')

nodups :: (Ord a) => [a] -> [a]
nodups [] = []
nodups xs = unique xs where
    removeDups []     = []
    removeDups (x:xs) = x:removeDups (dropWhile (==x) xs)
    unique xs = (removeDups . L.sort) xs

areNoDups :: (Eq a) => [a] -> Bool
areNoDups []     = True
areNoDups (x:xs) = all (/=x) xs && areNoDups xs
