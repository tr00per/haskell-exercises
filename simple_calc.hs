import Control.Monad.State

main = do
    putStrLn "Polish Notation Simple Calculator"
    line <- getLine
    let stack  = words line
        result = evalState calculate stack
    putStrLn $ show result

calculate :: State [String] Int
calculate = do
    xs:xss <- get
    if null xss
       then return (read xs::Int)
       else case xs of
            "+" -> execute (+)
            "*" -> execute (*)
            "-" -> execute (-)
            _   -> error (show xs)
    where
        execute f = do _:first:second:xss <- get
                       let result = op f first second
                       put (result:xss)
                       calculate
        op f first second = show $ (read first::Int) `f` (read second::Int)
