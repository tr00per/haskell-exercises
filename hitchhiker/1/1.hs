import System.IO (hFlush, stdout)

main = do
    putStr "What is your name?\n>"
    hFlush stdout
    name <- getLine
    putStrLn ("Hello, " ++ name)
