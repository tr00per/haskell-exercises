import System.IO (hFlush, stdout)

main = do
    putStr "Enter your name or type \"exit\" to finish\n>"
    hFlush stdout
    name <- getLine
    case name of
        "exit"    -> return ()
        otherwise -> do
            putStr $ "Hello, " ++ name ++ "! What is your favourite colour?\n>"
            hFlush stdout
            colour <- getLine
            putStrLn $ "So your name is " ++ name ++ " and your favourite colour is " ++ colour ++ "!\nThanks!"
            main
