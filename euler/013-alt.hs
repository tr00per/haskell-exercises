take10FromSum x = take 10 $ show $ sum x

readIntoList x = map (read :: String -> Integer) (lines x)

main = do
  fin <- (readFile "013-input.txt")
  putStrLn $ (take10FromSum . readIntoList) fin
