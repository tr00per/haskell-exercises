palindrome x = let digits = show x in digits == reverse digits
main = print (maximum [x * y | x <- [100..999], y <- [100..999], palindrome (x * y)])
