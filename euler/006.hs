problem6 limit = theSum * theSum - sumsq where
--         sumsq = sum [ n * n | n <- [1..limit] ]
--         sumsq = (sum . map (^2)) [1..limit]
         sumsq = sum (map (^2) [1..limit])
         theSum = sum [1..limit]
main = print (problem6 100)
