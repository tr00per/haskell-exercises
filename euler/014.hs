-- import Data.List

problem14 x = collatz (x, x, 1)

collatz (start, x, l)
    | x == 1    = (start, l)
    | even x    = collatz (start, div x 2, l + 1)
    | otherwise = collatz (start, 3 * x + 1, l + 1)

-- problem14' is ~1s faster than problem14
problem14' x = collatz' x x 1

collatz' start x l
    | x == 1    = (start, l)
    | even x    = collatz' start (div x 2) (succ l)
    | otherwise = collatz' start (3 * x + 1) (succ l)

maximumSequence [] = (0, -1)
maximumSequence (x:xs) = maxer xs x

maxer [] m = m
maxer (x:xs) m
          | snd x > snd m = maxer xs x
          | otherwise     = maxer xs m

main = print $ maximumSequence $ map problem14' [1..1000000]

-- Requires Data.List module and increased stack size, but does not run faster
-- main = print $ maximumBy (\a b -> compare (snd a) (snd b)) $ map problem14' [1..1000000]
