import Data.List

sumDigits :: String -> Integer
sumDigits x = sum $ map (read :: String -> Integer) (groupBy (\_ _ -> False) x)

main = print $ sumDigits (show $ 2 ^ 1000)
