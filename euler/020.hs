main = do
	let f = fac 100
	let s = sum $ map read $ map (:[]) (show f)
	putStrLn $ show s

fac 0 = 1
fac n = n * fac (n-1)
