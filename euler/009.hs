isTriplet a b c = a < b && b < c && a*a + b*b == c*c

triplets = [ a * b * c | a <- [1..1000], b <- [1..1000], c <- [1..1000], isTriplet a b c && a+b+c == 1000 ]

main = print $ head triplets
