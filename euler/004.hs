isPalindrome :: Int -> Bool
-- isPalindrome x = x == read (reverse (show x))
isPalindrome x = digits == reverse digits where digits = show x

main = print (maximum [ (x * y, x, y) | x <- [999,998..100], y <- [999,998..100], isPalindrome (x * y) ])
