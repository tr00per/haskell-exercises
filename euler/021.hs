import Data.List

main = putStrLn $ show . sum $ amicableNumbers

amicableNumbers :: [Integer]
amicableNumbers = nub . concat $ [[x, numPair x] | x <- [1..10000], x /= numPair x && x == dblNumPair x]
    where
        numPair x = sum $ divisors x
        dblNumPair x = numPair $ numPair x

divisors :: Integer -> [Integer]
divisors n = filter (\x -> (n `mod` x) == 0) [1 .. halfFloor n]
    where halfFloor x = toInteger . floor $ (fromInteger x) / 2
