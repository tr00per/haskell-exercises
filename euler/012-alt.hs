triangles = scanl1 (+) [1..]

getFactorsOf x = concat [ [n,x `div` n] | n <- [1..root x], mod x n == 0]
                 where
                   root x = ceiling $ sqrt $ fromIntegral x

factorPairs x = (x, length $ getFactorsOf x)

main = print $ fst . head $ dropWhile (\x -> snd x <= 500) (map factorPairs triangles)
