import Data.Numbers.Primes

-- ~ 4.35s
sumBelow x = sum [ n | n <- somePrimes, n < x ]
             where
               somePrimes = take halfX primes
               halfX = x `div` 2

-- ~ 0.5s
sumBelow2 x = sum $ takeWhile (< x) primes

main = print $ sumBelow2 2000000
