triangles = scanl1 (+) [1..]

getFactorsOf x = [ n | n <- [1..halfX], mod x n == 0] ++ [x] where halfX = div x 2 + 1

factorPairs x = (x, length $ getFactorsOf x)

main = print $ fst . head $ dropWhile (\x -> snd x <= 500) (map factorPairs triangles)
