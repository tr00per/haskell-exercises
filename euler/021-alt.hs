import Data.List

-- Taken from problem 12
primes = 2 : filter ((==1) . length . pfactors) [3,5..]
 
pfactors n = factor n primes
   where
       factor n (p:ps) 
            | p*p > n        = [n]
            | n `mod` p == 0 = p : factor (n `div` p) (p:ps)
            | otherwise      = factor n ps

amic x = sum.init $ nub $ map product $ subsequences $ pfactors x

isamic x = x /= ax && x == amic ax
    where
    ax = amic x

ans = sum $ filter isamic [2..9999]

main = putStrLn $ show ans
