(%/=) :: (Integral a) => a -> a -> Bool
a %/= b = mod a b /= 0

divisible :: (Integral a) => a -> [a] -> Bool
divisible _ [] = True
divisible target (d:divs)
    | target %/= d = False
    | otherwise	   = divisible target divs

willDiv _ [] = True
willDiv target (d:divs) = if mod target d == 0 then willDiv target divs else False

-- main = print (head [ n | n <- [1..], willDiv n [1..20]])
main = print (head [ n | n <- [20,22..], divisible n [3..20]])

-- divisiable + main#1 = 1m10.221s
-- willDiv + main#1 = 1m15.352s
-- willDiv + main#2 = 0m23.166s
-- divisible + main#2 = 0m21.770s
