evenFibSum n = sum $ filter even $ (takeWhile (<n) fibs) where
    fibs = 1 : 2 : zipWith (+) fibs (tail fibs)

main = print (evenFibSum 4000000)
