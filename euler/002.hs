fib 0 = 1
fib 1 = 1
fib n = (fib (n - 1)) + (fib (n - 2))

fibGen = [ fib n | n <- [1..]]
fibList = takeWhile (<4000000) fibGen
addends = [ n | n <- fibList, even n ]

main = print (sum addends)
