take10FromSum x = take 10 $ show $ sum x

readIntoList x = map (read :: String -> Integer) (lines x)

main = interact (take10FromSum . readIntoList)
