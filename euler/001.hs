import Data.List

a %= b = mod a b == 0

addLmod3or5 a b = if (b %= 3) || (b %= 5) then (a + b, b) else (a, b)

main = print (fst (mapAccumL addLmod3or5 0 [1..999]))
