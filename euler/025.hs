import Data.List

main = do
    let fib = 0:1:[a+b | (a,b) <- zip fib (tail fib)]
        e1k = head $ dropWhile (\x -> x < (10^999)) fib
    printElemIndex (elemIndex e1k fib)

printElemIndex (Just idx) = putStrLn $ show $ idx
printElemIndex Nothing = error "Element suddenly gone from list!"
