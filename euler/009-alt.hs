triplets abc = filter pythagorean [(a, b, abc - a - b) | a <- [1..abc], b <- [a + 1..abc], a + b < abc]
    where pythagorean (a, b, c) = a * a + b * b == c * c

solve = a * b * c
    where (a, b, c) = head $ triplets 1000

main = print solve
