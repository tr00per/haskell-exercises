import Data.Char as C
import Data.List as L

main = do
    namesStr <- readFile "022-input.txt"
    let names = sort $ lines $ normalizeList namesStr
        total = sum $ map (\x -> score x names) names
    putStrLn $ show total

normalizeList :: String -> String
normalizeList "" = ""
normalizeList (s:ss)
    | s == '"'  = normalizeList ss
    | s == ','  = '\n':normalizeList ss
    | otherwise = s:normalizeList ss

score :: String -> [String] -> Integer
score x xs = case L.elemIndex x xs of
                Just idx -> positiveIndex idx * alphaScore x 
                Nothing  -> error "Name vanished from the list!"
    where positiveIndex i = fromIntegral i + 1

alphaScore :: String -> Integer
alphaScore s = sum $ map (fromIntegral . convertAlpha . C.ord . C.toUpper) s
    where convertAlpha a = a - C.ord('A') + 1
