import Data.Numbers.Primes

type Range = (Integer, Integer)

main :: IO ()
main = do
    text <- getContents
    let _:ps = lines text
    mapM_ printRange $ map unpackRange ps

unpackRange :: String -> Range
unpackRange = makeRangePair . words

makeRangePair :: [String] -> Range
makeRangePair (min:max:[]) = (read min, read max)
makeRangePair _ = error "Wrong input format"

printRange :: Range -> IO ()
printRange (min,max) = putStrLn $ showInLines $ primesInRange min max

showInLines :: [Integer] -> String
showInLines = unlines . map (\x -> show x)

primesInRange :: Integer -> Integer -> [Integer]
primesInRange min max = takeWhile (\x -> x <= max) $ dropWhile (\x -> x < min) $ primes
