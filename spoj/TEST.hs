#!/usr/bin/runhaskell
main = do
    text <- getContents
    let filtered = unlines $ takeWhile (\x -> (read x::Integer) /= 42) $ lines text
    putStrLn filtered
