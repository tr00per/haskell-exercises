import qualified Data.List as L

main = do
    let m1 = Matrix [[1,2,3],[4,5,6],[7,8,9]]
    putStrLn $ show m1 ++ "\n"
    let m2 = transpose m1
    putStrLn $ show m2 ++ "\n"
    let m3 = Matrix [[1,0,0],[0,1,0],[0,0,1]]
    putStrLn $ show m3 ++ "\n"
    let m4 = m1 + m3
    putStrLn $ show m4 ++ "\n"
    let m5 = m1 * m3
    putStrLn $ show m5 ++ "\n"
    let m6 = m1 * m1
    putStrLn $ show m6 ++ "\n"
    let detM1 = determinant m1
    putStrLn $ show detM1
    let detM2 = determinant m2
    putStrLn $ show detM2
    let detM3 = determinant m3
    putStrLn $ show detM3

newtype Matrix a = Matrix [[a]] deriving (Eq, Ord)

instance (Show a) => Show (Matrix a) where
    show = showMatrix

showMatrix :: (Show a) => Matrix a -> String
showMatrix (Matrix xss) = L.intercalate "\n" . map (L.intercalate " " . map show) $ xss

transpose :: Matrix a -> Matrix a
transpose (Matrix xss) = Matrix (L.transpose xss)

instance (Num a) => Num (Matrix a) where
    (+) (Matrix xss) (Matrix yss) = Matrix $ zipWith (zipWith (+)) xss yss
    (-) (Matrix xss) (Matrix yss) = Matrix $ zipWith (zipWith (-)) xss yss
    (*) (Matrix xss) (Matrix yss) = Matrix $ [map (\ys -> sum (zipWith (*) xs ys)) yss' | xs <- xss] where
        yss' = L.transpose yss
    negate (Matrix xss) = Matrix $ map (map negate) xss
    abs _ = error "Unsupported operation"
    signum _ = error "Unsupported operation"
    fromInteger _ = error "Unsupported operation"

determinant :: (Num a) => Matrix a -> a
determinant (Matrix xss) = getDeterminant xss

getDeterminant :: (Num a) => [[a]] -> a
getDeterminant []  = 1
getDeterminant xss = sum [(-1)^i * x * (getDeterminant (getMinor i xss)) | (i,x) <- zip [0..] (head xss)]

getMinor :: Int -> [[a]] -> [[a]]
getMinor i xs = map ((\(pre, _:post) -> pre ++ post) . splitAt i) (tail xs)
